import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import '@fortawesome/react-fontawesome'
import 'bootstrap/scss/bootstrap.scss'
import 'bootstrap/dist/js/bootstrap'

ReactDOM.render(<App />, document.getElementById('root'))
