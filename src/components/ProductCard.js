import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faClosedCaptioning } from '@fortawesome/free-solid-svg-icons'
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import ProductSDV from '../components/ProductSDV'
import Slider from '../components/Slider'

const ProductCard = () => {
  return (
    <div className="container product-card">
      {/*BREADCRUMBS*/}
      <nav className="breadcrumbs-box">
        <ul className="breadcrumbs reset-list">
          <li className="breadcrumbs__item">
            <a className="reset-link" href="#">
              Home
            </a>
            <span className="breadcrumbs__arrow">
              <FontAwesomeIcon icon={faArrowLeft} />
            </span>
          </li>
          <li className="breadcrumbs__item active">
            <a className="reset-link" href="#">
              Products
            </a>
          </li>
        </ul>
      </nav>
      {/*/BREADCRUMBS*/}
      <ProductSDV />
      <div className="row">
        <div className="col-12">
          <div className="card-tabs">
            <ul className="card-tabs__head reset-list">
              <li>
                <a href="#tab1" className="js-card-tab-link card-tabs__link active">
                  Overview
                </a>
              </li>
              <li>
                <a href="#tab2" className="js-card-tab-link card-tabs__link">
                  Delivery
                </a>
              </li>
              <li>
                <a href="#tab3" className="js-card-tab-link card-tabs__link">
                  payment
                </a>
              </li>
              <li>
                <a href="#tab4" className="js-card-tab-link card-tabs__link">
                  Reviews
                </a>
              </li>
            </ul>
            <div className="card-tabs__body">
              <div className="js-card-tab-item card-tabs__item" id="tab1">
                <div className="card-characteristics">
                  <p className="card-characteristics__desc">
                    Prosthetic Legs for Athletes is a high quality, photo real model that will
                    enhance detail and realism to any of your rendering projects. The model has a
                    fully textured, detailed design that allows for close-up renders, and was
                    originally modeled in 3ds Max 2012 and rendered with V-Ray. Renders have no
                    postprocessing.
                  </p>
                  <div className="row">
                    <div className="col-12 col-xl-8">
                      <table className="card-characteristics__table">
                        <thead>
                          <tr>
                            {/*-<th width="240">ХАРАКТЕРИСТИКА</th>*/}
                            <th>Features:</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                              High quality polygonal model, correctly scaled for an accurate
                              representation of the original object
                            </td>
                          </tr>
                          <tr>
                            <td>
                              Models resolutions are optimized for polygon efficiency. (In 3ds Max,
                              the Meshsmooth function can be used to increase mesh resolution if
                              necessary.)
                            </td>
                          </tr>
                          <tr>
                            <td>Model is fully textured with all materials applied</td>
                          </tr>
                          <tr>
                            <td>
                              All textures and materials are included and mapped in every format
                            </td>
                          </tr>
                          <tr>
                            <td>
                              3ds Max models are grouped for easy selection, and objects are
                              logically named for ease of scene management
                            </td>
                          </tr>
                          <tr>
                            <td>
                              No part-name confusion when importing several models into a scene
                            </td>
                          </tr>
                          <tr>
                            <td>
                              No cleaning up necessaryjust drop your models into the scene and start
                              rendering
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div className="js-card-tab-item card-tabs__item" id="tab2">
                some content
              </div>
              <div className="js-card-tab-item card-tabs__item" id="tab3">
                some content 2
              </div>
              <div className="js-card-tab-item card-tabs__item" id="tab4">
                some content 3
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          <h2 className="product-card__title-same-slider head-chapter">Similar model</h2>
        </div>
        <div className="col-12 offset-0 col-md-10 offset-md-1">
          {/*SAME PROD SLIDER */}
          <Slider />
          {/*/SAME PROD SLIDER */}
        </div>
      </div>
      <div className="add-to-bag">
        <div className="add-to-bag__layout js-add-to-bag-layout"></div>
        <div className="add-to-bag__body js-add-to-bag-body">
          <a href="#" className="add-to-bag__close js-add-to-bag-close">
            <FontAwesomeIcon icon={faClosedCaptioning} />
          </a>
          <div className="add-to-bag__head">
            <span>КОРЗИНА</span>
            <span>(1)</span>
          </div>
          <hr />
          <div className="add-to-bag__prod-box">
            <a href="#" target="_blank" className="product-min reset-link">
              <div className="product-min__img">
                <span className="product-min__label product-min__label--promotion">
                  <span>АКЦИЯ</span>
                </span>
                <img src="http://via.placeholder.com/85x104" />
              </div>
              <div className="product-min__box">
                <h1 className="product-min__title">Prosthetic Legs for Athletes 3D modelby</h1>
                <div className="product-min__sizes-box">
                  <span>24</span>
                  <span>белый</span>
                </div>
                <p className="product-min__price">610 грн</p>
              </div>
            </a>
          </div>
          <div className="add-to-bag__bottom">
            <div className="add-to-bag__bottom-row">
              <span className="add-to-bag__name-row">Delivery:</span>
              <span className="add-to-bag__total-row">0 грн.</span>
            </div>
            <div className="add-to-bag__bottom-row">
              <span className="add-to-bag__name-row">Delivery:</span>
              <span className="add-to-bag__total-row">610 грн.</span>
            </div>
            <div className="add-to-bag__bottom-btn">
              <button className="btn--popup">ОФОРМИТЬ ЗАКАЗ</button>
              <button className="btn--popup btn--popup__tr">ВЕРНУТЬСЯ К ПОКУПКАМ</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ProductCard
