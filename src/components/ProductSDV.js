import React, { useCallback, useRef } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft, faShare } from '@fortawesome/free-solid-svg-icons'
import initSdvApp from '../helpers/SdvHelper'

const ProductSDV = () => {
  const ticket =
    'e58e204f42aaf9b827b72fc8e0e184ed69c4020cd19e28c19ab3417555dabb6347129bde7f58b831b59aa5aad738c35c89d3d70e02df1f8271b46e5185665f555475a677e0170347924a6b02248b55d48f9cf16f23596d685ae39dad941cb3cc7d8ccbc42e96ffca93c9160aa0a6ec1a63f701b42b90-36c35d0758d5aaae43f92aa7bf9b0a9f'
  const testRef = useRef(null)
  const sdvRef = useCallback(node => {
    if (node !== null) {
      document.addEventListener('DOMContentLoaded', () => initSdvApp(node, testRef, ticket), false)
    }
  }, [])

  return (
    <div className="row">
      <div className="col-12 col-lg-5 col-xl-6">
        <div className="mob-card-head">
          <div className="mob-breadcrumbs">
            <FontAwesomeIcon icon={faArrowLeft} />
            <a className="mob-breadcrumbs__link" href="#">
              обувь для танцев
            </a>
          </div>
          <h2 className="mob-card-head__title">Prosthetic Legs for Athletes 3D modelby</h2>
        </div>
        <div id="sdv-container" ref={sdvRef} />
        <span className={'hint'}>* Hold and drag to rotate</span>
        <div className="product-card__social">
          <span>Share:</span>
          {/*SOCIAL BTN*/}
          <div className="c-social">
            <a href="#" target="_blank">
              <FontAwesomeIcon icon={faShare} />
            </a>
            <a href="#" target="_blank">
              {/*<FontAwesomeIcon icon={faGooglePlus} />*/}
            </a>
          </div>
          {/*SOCIAL BTN */}
        </div>
      </div>
      <div className="col-12 col-md-8 offset-md-2 col-lg-7 offset-lg-0 col-xl-6">
        <div className="card-head">
          <h1 className="card-head__title">Prosthetic Legs for Athletes 3D modelby</h1>
          <div className="card-head__price">
            <div className="card-head__old-price">2500 $.</div>
            <div className="card-head__current-price">2255 $.</div>
          </div>
        </div>
        <div className="card-size">
          <div ref={testRef} id="parameters"></div>
        </div>
        <div className="card-buy">
          <div className="row">
            <div className="col-5 card-buy__count">
              <span className="card-buy__count-title">Qty</span>
              {/*COUNTER INPUT*/}
              <div className="c-counter js-counter-box">
                <span className="c-counter__btn js-dec js-count-btn">-</span>
                <input className="c-counter__input js-count-input" type="text" />
                <span className="c-counter__btn js-inc js-count-btn">+</span>
              </div>
              {/*/COUNTER INPUT*/}
            </div>
            <div className="col-7 col-sm-6 col-lg-5">
              <button className="btn--popup card-buy__btn js-call-add-to-bag">Add to cart</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ProductSDV
