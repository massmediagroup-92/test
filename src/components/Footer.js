import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPhone } from '@fortawesome/free-solid-svg-icons'
import { faLocationArrow } from '@fortawesome/free-solid-svg-icons'
import { faMailBulk } from '@fortawesome/free-solid-svg-icons'
import { faShare } from '@fortawesome/free-solid-svg-icons'

const Footer = () => (
  <footer className="footer">
    <div className="container-fluid">
      <div className="row footer-top align-items-center">
        <div className="col-sm-12 col-lg-12 footer-top__box">
          <div className="row">
            <div className="col-12 col-sm-3 col-md-4 col-lg-3 footer-menu">
              <ul className="footer-menu__box">
                <li>
                  <a href="#">About</a>
                </li>
                <li>
                  <a href="#">Delivery and payment</a>
                </li>
                <li>
                  <a href="#">Contact us</a>
                </li>
              </ul>
            </div>
            <div className="col-12 col-sm-5 col-md-4 col-lg-5 footer-contacts">
              <ul className="footer-contacts__box">
                <li className="footer-contacts__item">
                  <span className="footer-contacts__item-img">
                    <FontAwesomeIcon icon={faLocationArrow} />
                  </span>
                  <span>3600 Walnut Street, Kansas City, MO 64111</span>
                </li>
                <li className="footer-contacts__item">
                  <span className="footer-contacts__item-img">
                    <FontAwesomeIcon icon={faPhone} />
                  </span>
                  <div className="footer-contacts__item-tel">
                    <a href="tel:+380673118191">+38 067 311 81 91</a>
                    <a href="tel:+380950387071">+38 095 038 70 71</a>
                  </div>
                </li>
                <li className="footer-contacts__item">
                  <span className="footer-contacts__item-img">
                    <FontAwesomeIcon icon={faMailBulk} />
                  </span>
                  <a href="mailto:hello@shapediver.com">hello@shapediver.com</a>
                </li>
              </ul>
            </div>
            <div className="col-12 col-sm-4 col-md-4 col-lg-4 footer-info">
              <ul className="footer-info__box">
                <li className="footer-info__item">
                  <div className="footer-info__work-time">
                    <p>Mon-Fr: 9.00 - 18.00,</p>
                    <p>Sat: 9.00 - 15.00, Sun: day off</p>
                  </div>
                </li>
                <li className="footer-info__item">
                  <div className="footer-info__tel tel-collapse">
                    <div className="tel-collapse__head" id="headingOne">
                      <span
                        data-toggle="collapse"
                        data-target="#collapseOne"
                        aria-expanded="true"
                        aria-controls="collapseOne"
                      >
                        +38 067 311 81 91
                      </span>
                    </div>
                    <div
                      id="collapseOne"
                      className="tel-collapse__body collapse"
                      aria-labelledby="headingOne"
                    >
                      <a href="tel:+380673118191">+38 095 038 70 71</a>
                      <a href="tel:+380950387071">+38 095 038 70 71</a>
                    </div>
                  </div>
                </li>
                <li className="footer-info__item">
                  <div className="footer-info__call-back">
                    <a href="#">Сallback</a>
                  </div>
                </li>
                <li className="footer-info__item">
                  <div className="footer-info__social">
                    <a href="#" target="_blank" className="footer-info__social-img">
                      <FontAwesomeIcon icon={faMailBulk} />
                    </a>
                    <a href="#" target="_blank" className="footer-info__social-img">
                      <FontAwesomeIcon icon={faShare} />
                    </a>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
)

export default Footer
