import React from 'react'

const ProductItem = () => (
  <div className="same-prod-slider__item-box">
    <div className="same-prod-slider__item">
      <a href="#" className="product-item reset-link">
        <div className="product-item__img">
          <img src="http://via.placeholder.com/260x350" />
        </div>
        <div className="product-item__content">
          <h3 className="product-item__title">Prosthetic Legs for Athletes 3D modelby</h3>
          <div className="product-item__price-box">
            <span className="product-item__current-price">69 $.</span>
            <span className="product-item__old-price">89 $.</span>
          </div>
        </div>
      </a>
    </div>
  </div>
)

export default ProductItem
