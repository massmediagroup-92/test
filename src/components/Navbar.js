import React from 'react'
import LogoImage from '../images/logo.jpg'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHeart } from '@fortawesome/free-solid-svg-icons'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'
import { faFlag } from '@fortawesome/free-solid-svg-icons'
import { faArrowDown } from '@fortawesome/free-solid-svg-icons'
import { faSearch } from '@fortawesome/free-solid-svg-icons'

const NavBar = () => (
  <div>
    <header className="header">
      <div className="container">
        <div className="row header-top">
          <div className="col-md-8 header-top__info-box">
            <div className="row">
              <div className="col-md-6 header-text">
                Mon-Fr: 9.00 - 18.00, Sat: 9.00 - 15.00, Sun: day off
              </div>
              <div className="col-md-6 header-top__nav-box">
                <ul className="header-top__nav">
                  <li className="header-top__nav-item">
                    <a href="#" className="header-text">
                      About
                    </a>
                  </li>
                  <li className="header-top__nav-item">
                    <a href="#" className="header-text">
                      FAQs
                    </a>
                  </li>
                  <li className="header-top__nav-item">
                    <a href="#" className="header-text">
                      Contact us
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="col-md-4 header-top__user">
            <div className="row">
              <div className="col-md-7">
                <a
                  href="#"
                  className="header-top__log-in header-text"
                  data-fancybox
                  data-src="#enter-pass"
                >
                  Sign in/Log in
                </a>
              </div>
              <div className="col-md-5 padding_for_arrow">
                <div className="header-top__icon-box">
                  <a className="user-like" href="#">
                    <FontAwesomeIcon icon={faHeart} />
                  </a>
                  <a className="user-cart" href="#">
                    <div className="user-cart__circle">
                      <span className="user-cart__count">15</span>
                    </div>
                    <FontAwesomeIcon icon={faShoppingCart} />
                  </a>
                  <div className="lang js-lang">
                    <div className="lang__default">
                      <FontAwesomeIcon icon={faFlag} />
                      <FontAwesomeIcon icon={faArrowDown} />
                    </div>
                    <div className="js-append-lang"></div>
                    <ul className="lang__box reset-list">
                      <li className="lang__item">
                        <FontAwesomeIcon icon={faFlag} />
                        <a href="#">Укр</a>
                      </li>
                      <li className="lang__item">
                        <FontAwesomeIcon icon={faFlag} />
                        <a href="#">Рус</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row header-bottom">
          <div className="col-md-2 col-xl-3">
            <a href="#" className="logo">
              <img src={LogoImage} alt="Logo" />
            </a>
          </div>
          <div className="col-md-10 col-xl-9">
            <div className="row header-bottom__content">
              <div className="col-md-9 nav-box">
                <div className="search">
                  <div className="search__body js-search-body">
                    <input className="search__input" type="tet" placeholder="Search ..." />
                    <button className="search__go">
                      <FontAwesomeIcon icon={faSearch} />
                    </button>
                  </div>
                  <span className="faSearch__toggle js-search-body">
                    <FontAwesomeIcon icon={faSearch} />
                  </span>
                </div>
              </div>
              <div className="col-md-3 text-right">
                <div className="header-tel-collapse">
                  <div
                    className="header-tel-collapse__head"
                    id="headingTwo"
                    data-toggle="collapse"
                    data-target="#collapseTwo"
                    aria-expanded="true"
                    aria-controls="collapseTwo"
                  >
                    <span>+38 067 311 81 91</span>
                    <span className="header-tel-collapse__arrow">
                      <FontAwesomeIcon icon={faArrowDown} />
                    </span>
                    <div
                      id="collapseTwo"
                      className="header-tel-collapse__body collapse"
                      aria-labelledby="headingTwo"
                    >
                      <span>+38 095 038 70 71</span>
                      <a href="#">+38 095 038 70 71</a>
                      <a className="header-tel-collapse__link" href="#">
                        Сallback
                      </a>
                    </div>
                  </div>
                </div>
                <a className="header-bottom__call-back" href="#">
                  Сallback
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    <header className="header-mob">
      <div className="container-fluid">
        <div className="row header-top">
          <div className="col-md-12 header-top__user">
            <div className="row">
              <div className="col-7 col-md-9">
                <a href="#" className="header-top__log-in header-text">
                  Sign in/Log in
                </a>
              </div>
              <div className="col-5 col-md-3">
                <div className="header-top__icon-box">
                  <a className="user-like" href="#">
                    <FontAwesomeIcon icon={faHeart} />
                  </a>
                  <a className="user-cart" href="#">
                    <div className="user-cart__circle">
                      <span className="user-cart__count">15</span>
                    </div>
                    <FontAwesomeIcon icon={faShoppingCart} />
                  </a>
                  <div className="lang js-lang">
                    <div className="lang__default">
                      <FontAwesomeIcon icon={faFlag} />
                      <FontAwesomeIcon icon={faArrowDown} />
                    </div>
                    <div className="js-append-lang"></div>
                    <ul className="lang__box reset-list">
                      <li className="lang__item">
                        <FontAwesomeIcon icon={faFlag} />
                        <a href="#">Укр</a>
                      </li>
                      <li className="lang__item">
                        <FontAwesomeIcon icon={faFlag} />
                        <a href="#">Рус</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row header-bottom">
          <div className="col-10 col-sm-11 logo-box">
            <a href="#" className="logo">
              <img src={LogoImage} alt="Logo" />
            </a>
            <div className="search">
              <div className="search__body el--invisible js-search-body">
                <input className="search__input" type="text" placeholder="Search ..." />
                <button className="search__go">
                  <FontAwesomeIcon icon={faSearch} />
                </button>
              </div>
              <span className="search__toggle js-search-toggle">
                <FontAwesomeIcon icon={faSearch} />
              </span>
            </div>
          </div>
        </div>
      </div>
    </header>
  </div>
)

export default NavBar
